from django.template import loader
from notasquare.urad_web.renderers import BaseRenderer

class FullPageContextRenderer(BaseRenderer):
    def __init__(self):
        super(FullPageContextRenderer, self).__init__()
        self.template = 'sequence/page_contexts/full-demo.html'
    def render(self, full_page_context):
        template = loader.get_template(self.template)
        context = {}
        context['session'] = full_page_context.session
        context['header_title'] = "SEQUENCE VIEWER"
        context['page_title'] = "Genopedia"
        context['menu'] = [
            {
                'url' : '/variation/view',
                'name': 'Sequence Viewer'
            },
            {
                'url' : '/gene/view',
                'name': 'Gene Viewer'
            },
            {
                'url' : '/integration_guide/view',
                'name': 'Integration guide'
            }
        ];
        widget_html = ''
        for widget in full_page_context.widgets:
            widget_html += widget.render()
        context['widget_html'] = widget_html

        return template.render(context)
