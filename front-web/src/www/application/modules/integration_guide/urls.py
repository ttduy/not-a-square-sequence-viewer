
from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^$',                              actions.IntegrationGuide.as_view(),     name='integration_guide_view'),
    url(r'^view$',                              actions.IntegrationGuide.as_view(),     name='integration_guide_view')
]
