from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.template import loader
from notasquare.urad_web import actions, widgets, renderers
from application.themes.sequence import renderers as sequence_renderers
from application.modules.common import page_contexts, actions as common_actions, components as common_components
from application import constants

class IntegrationGuide(common_actions.BaseAction):
    class StatisticsWidget(widgets.BaseWidget):
        def __init__(self):
            pass
    class StatisticsWidgetRenderer(renderers.BaseRenderer):
        def render(self, statistics_widget):
            template = loader.get_template('sequence/integration_guide.html')
            context = {}
            context['heading'] = "Integration Guide"
            context['url'] = settings.HOST_URL_LINK
            return template.render(context)
    def create_statistics_widget(self):
        statistics_widget = self.StatisticsWidget()
        statistics_widget.renderer = self.StatisticsWidgetRenderer()
        statistics_widget.num_variation = 0
        return statistics_widget
    def GET(self):
        page_context = self.create_page_context()
        statistics_widget = self.create_statistics_widget()
        page_context.add_widget(statistics_widget)
        return HttpResponse(page_context.render())
