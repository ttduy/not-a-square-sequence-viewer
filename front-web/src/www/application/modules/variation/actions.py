from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.template import loader
from notasquare.urad_web import actions, widgets, renderers
from application.themes.sequence import renderers as sequence_renderers
from application.modules.common import page_contexts, actions as common_actions, components as common_components
from application import constants
from . import components
import json
#------------------------------

class Variation(common_actions.BaseAction):
    class StatisticsWidget(widgets.BaseWidget):
        def __init__(self):
            pass
    class StatisticsWidgetRenderer(renderers.BaseRenderer):
        def render(self, statistics_widget):
            template = loader.get_template('sequence/variation-demo.html')
            context = {}
            context['url'] = "http://127.0.0.1:8022/variation.html?rsnumber="
            context['heading'] = "Variation"
            context['data'] = []
            context['data'].append({
                    'data':     statistics_widget.data,
                    'url_api': settings.DOMAIN_API,	
		    'genome_url': settings.GENOME_BROWSER_URL	#set genome browser url
                })
            return template.render(context)
    def create_statistics_widget(self):
        statistics_widget = self.StatisticsWidget()
        statistics_widget.renderer = self.StatisticsWidgetRenderer()
        statistics_widget.num_variation = 0
        return statistics_widget
    def GET(self):
        page_context = self.create_page_context()
        statistics_widget = self.create_statistics_widget()
        statistics_widget.data = []     #replace {} by []
        if 'rsnumber' in self.params:
            if components.Handle(self.get_container()).get(self.params['rsnumber'])['status'] == 'ok':
                statistics_widget.data = json.dumps(components.Handle(self.get_container()).get(self.params['rsnumber'])['data']['record'])

        page_context.add_widget(statistics_widget)
        return HttpResponse(page_context.render())
