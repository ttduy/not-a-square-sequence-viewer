
from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^view$',      actions.Variation.as_view(),     	name='variation_view'),
    # url(r'^get$',    	actions.GetData.as_view(),      	 name='variation_view')
    url(r'^get$',    	actions.Variation.as_view(),      	 name='variation_get')
]
