from django.conf import settings
from application.modules.common import page_contexts

#insert class Handle
class Handle(object):
    def __init__(self, container):
        self.container = container
    #get data from rsnumber (id)
    def get(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/get', GET={'rsnumber': id})
    #dinh nghia phuong thuc load dua theo formule cua phuong thuc get o tren
    def load(self, id, start, end):
     	return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/load', GET={'rsnumber': id, 'start': start, 'end': end})
