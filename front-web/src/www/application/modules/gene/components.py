from django.conf import settings
from application.modules.common import page_contexts

#insert class Handle
class Handle(object):
    def __init__(self, container):
        self.container = container
    #get data from rsnumber (id)
    def get(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/gene/get', GET={'gene': id})
