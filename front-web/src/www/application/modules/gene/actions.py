from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.template import loader
from notasquare.urad_web import actions, widgets, renderers
from application.themes.sequence import renderers as sequence_renderers
from application.modules.common import page_contexts, actions as common_actions, components as common_components
from application import constants
#___________ components = ?
from . import components
import json

class Gene(common_actions.BaseAction):
    class StatisticsWidget(widgets.BaseWidget):
        def __init__(self):
            pass
    class StatisticsWidgetRenderer(renderers.BaseRenderer):
        def render(self, statistics_widget):
            template = loader.get_template('sequence/gene-demo.html')
            context = {}
            context['url'] = "http://test.sequence.dev.genopedia.com/gene.html?gene="
            context['heading'] = "Gene"
            # context['data']    = statistics_widget.data
            #context['url_api'] = settings.DOMAIN_API
            context['data'] = []
            context['data'].append({
                    'data':     statistics_widget.data,
                    'url_api': settings.DOMAIN_API,
                    'genome_url': settings.GENOME_BROWSER_URL
                })
            return template.render(context)
    def create_statistics_widget(self):
        statistics_widget = self.StatisticsWidget()
        statistics_widget.renderer = self.StatisticsWidgetRenderer()
        statistics_widget.num_variation = 0
        return statistics_widget
    def GET(self):
        page_context = self.create_page_context()
        statistics_widget = self.create_statistics_widget()
        statistics_widget.data = []
        if 'gene' in self.params:
            if components.Handle(self.get_container()).get(self.params['gene'])['status'] == 'ok':
                statistics_widget.data = json.dumps(components.Handle(self.get_container()).get(self.params['gene'])['data']['record'])
        page_context.add_widget(statistics_widget)
        return HttpResponse(page_context.render())
