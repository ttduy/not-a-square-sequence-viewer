
from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^view$',                         actions.Gene.as_view(),     name='gene_view'),
    url(r'^get$',    					   actions.Gene.as_view(),     name='gene_get')
]
