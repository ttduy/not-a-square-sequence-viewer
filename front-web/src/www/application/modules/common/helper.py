from django.conf import settings
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.template import Context


class Helper(object):
    
    def send_email(self, **data): 
        c = Context(data['params']) 
        html_content = render_to_string('sequence/home/reset_passwrod_mail_template.html', c)
        
        email = EmailMultiAlternatives(data['subject'], settings.EMAIL_HOST_USER)
        email.attach_alternative(html_content, "text/html")
        email.to = data['recievers']
        email.send()


    def generate_reset_pass_link(self, user_id, token):
        return '{}/user/reset-password/confirm?id={}&token={}'.format(settings.LINK_GENOPEDIA, user_id, token)
