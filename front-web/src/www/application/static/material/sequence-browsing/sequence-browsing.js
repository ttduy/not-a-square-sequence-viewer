jQuery(document).ready(function() {
    var variation = jQuery('#sequence-browser-variation');
    if (variation.length) {
        ReactDOM.render(
           React.createElement(BannerVariationComponent,{}), document.getElementById('sequence-browser-variation')
        );
    }
    var gene = jQuery('#sequence-browser-gene');
    if (gene.length) {
        ReactDOM.render(
           React.createElement(BannerGeneComponent,{}), document.getElementById('sequence-browser-gene')
        );
    }

});

var BannerVariationComponent = React.createClass({
 getInitialState: function () {
   return {
     data: {
       variation : [],
       parameters : [],
       sequence: ''
     }
   }
 },
 componentWillMount(){
   this.handleBlockUI();
 },
 componentDidMount: function() {
    rsnumber = this.getURLParameter('rsnumber');
    if (rsnumber == '') rsnumber = 'rs0';
        this.handleLoadData(rsnumber);
   },
 handleBlockUI: function() {
        var root = this;
        jQuery("#sequence-browser-variation").block({
            message: "<img src='/img/loading.gif' />",
            css: {
                border: 'none',
                backgroundColor: 'argb(255,255,255,255)',
                marginTop: '50px'
            },
            fadeIn: 0,
            fadeOut: 0,
            overlayCSS:  {
                backgroundColor: '#FFF',
                opacity:         1,
                cursor:          'wait'
            },
        });
    },
  getURLParameter:function(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }
    return(false);
 },
 handleLoadData: function(rsnumber){
   var _this = this;
   jQuery.ajax({
        type: "get",
        dataType: 'json',
        crossDomain: true,
        url: 'http://sequence-viewer-front.genopedia.local/variation/get?rsnumber='+rsnumber,
        async: false,
        success: function (result) {
          _this.setState({
                  data: result['data']['record']
                });
          // $.unblockUI()
          jQuery("#sequence-browser-variation").unblock();
        },
        error: function (result) {
        }
    });
 },
 renderSequencePointer: function (variables) {
    var styleText = {height: '33px', 'marginTop': '-33px', 'fontWeight': 'bold','position':'absolute', 'cursor': 'pointer'};
    var elements = [];
    if (variables) {
        if (variables.sequence != '') {
          var mappingLetter = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G'};
          var str = variables.sequence;
          var stringTop = str!=null ? str.split(""):[];
          var mapping = ['Short', 'Med', 'Long', 'Long', 'Med', 'Short'];
          var index = 0;
          var data = this.handlePosition(variables.parameters);

          var exonStart = data[0];
          var exonEnd = data[1];
          var snpPosition = data[2];

          var variation = variables.variation;

          for(i = data[0]; i < data[1]; i++){
            var letterTop = stringTop[index];
            var letterBottom = mappingLetter[letterTop];
            var root = this;

            var attrBehind = {};
            attrBehind.key = i;
            attrBehind.className = i;

            var attrLetterAbove = {};
            attrLetterAbove.className = 'letter';

            var attrLetterBottom = {};
            attrLetterBottom.className = 'letter letter-gray';

            var attrSequence = {};
            attrSequence.key = i;
            attrSequence.className = 'sequence';

            var attr = {};
            attr.key = i;
            attr.className = 'snp-pointer';
            attr.className = attr.className + ' icon-Gen'+mapping[i%6];

            element = React.createElement('div', {},
                          React.createElement('div', {style: styleText}, rs),
                          React.createElement('div', attr)
                      )

            element = React.createElement('div', attr);
            // Exon
            if (i >= exonStart && i <= exonEnd) {
                attr.className = attr.className +' pointer-hightlight';
                element = React.createElement('div', attr);

                for (j = 0; j < variation.length; j++) {

                    var eleTooltip = '';
                    eleTooltip = 'Chromosome: '+ variation[j]['chromosome'] + "\n"+ 'Position: '+ variation[j]['position'] + "\n" + 'Gene: '+ variation[j]['gene']+ "\n"+ 'Associated diseases:'+ variation[j]['associated_disease'];

                    var j_position = Math.floor(variation[j]['position'] - variables.parameters.start);
                    if (j_position == i && snpPosition == i) {
                        var color = ' snp-mutation-hightlight ';
                        attr.className = attr.className + color + ' variation-pointer';
                        attrLetterAbove.className = attrLetterAbove.className + ' letter-mutation';
                        var rs = variation[j]['rsnumber'];
                        // element = React.createElement('div', attr);
                        element = React.createElement('div', {},
                                        React.createElement('div', {
                                                style: styleText,
                                                className: 'dropdown',
                                                'data-toggle': 'tooltip',
                                                'data-placement': 'bottom',
                                                'data-html': 'true',
                                                'title': eleTooltip
                                            },
                                            rs
                                        ),
                                        React.createElement('div', attr)
                                  )
                        break;
                    }else if (j_position == i) {
                        var color = ' snp-mutation-hightlight-bonus ';
                        attr.className = attr.className + color + ' variation-pointer';
                        attrLetterAbove.className = attrLetterAbove.className + ' letter-mutation-bonus';
                        var rs = variation[j]['rsnumber'];
                        // element = React.createElement('div', attr);
                        element = React.createElement('div', {},
                                        React.createElement('div', {
                                                style: styleText,
                                                className: 'dropdown',
                                                'data-toggle': 'tooltip',
                                                'data-placement': 'bottom',
                                                'data-html': 'true',
                                                'title': eleTooltip
                                            },
                                            rs
                                        ),
                                        React.createElement('div', attr)
                                  )
                        break;
                    }
                }
             } //----for
             elements.push(
               React.createElement('div', attrSequence,
                 React.createElement('div', attrBehind,
                   React.createElement('div', attrLetterAbove, letterTop),
                   element,
                   React.createElement('div', attrLetterBottom, letterBottom)
                 )
               )
             );
             index++;
          }
        }
    }
    return elements;
 },
 handlePosition: function(exon) {
   var terminal = Math.floor(exon.end - exon.start);
   var exonStart = 0;
   var exonEnd = terminal;
   var snpPosition = (exon.position - exon.start);

 return [exonStart, exonEnd, snpPosition];
},
handleAjaxLoadSequence:function(pos) {

  if ( pos > 0 ) {
      var start = this.state.data.parameters.end + 1;
      var end = start + 200;
  } else {
      var end = this.state.data.parameters.start - 1;
      var start = end - 200;
  }

  var parent = this;

  var rs = [];
  var url = 'http://api.sequence.dev.genopedia.com/sequences/load/'+rsnumber+'/'+start+'/'+end
  jQuery.ajax({
      type: "get",
      dataType: 'json',
      url: url,
      async: false,
      success: function (result) {
          rs = result['data']['record'];
      },
      error: function (result) {
      }
   });
  _current_state = this.state.data;
  var min = Math.min(_current_state.parameters.start, rs['parameters']['start']);
  var max = Math.max(_current_state.parameters.end, rs['parameters']['end']);
  _current_state.parameters.start = min;
  _current_state.parameters.end = max;
  if (pos > 0 ) { // next
      _current_state.variation = rs['variation'].concat(_current_state.variation);
      _current_state.sequence += rs['sequence'];

  }else{ // previous
      _current_state.variation = (_current_state.variation).concat(rs['variation']);
      _current_state.sequence = rs['sequence'] + _current_state.sequence;
  }
    this.setState({data: _current_state});
},
handleMoveNext: function() {
 var distance = $('.sequence:last').offset().left - $('.variation-bar').offset().left - 450;
 if (distance <= $('.variation-bar').width()) {
   this.handleAjaxLoadSequence(1000);
 }
 var leftPos = $('.variation-bar').scrollLeft()+450;
 $(".variation-bar").stop().animate({scrollLeft: leftPos }, 400);
},
handleMoveBack: function() {
//  this.handleAjaxLoadSequence(-1000);
var leftPos = $('.variation-bar').scrollLeft();
$(".variation-bar").stop().animate({scrollLeft: leftPos - 450}, 400);

},
renderControlPanel: function () {
 return (
   React.createElement('div', {className: 'sequenceControlBtnGroup'},
     React.createElement('div', {className: 'rightGroup'},
       React.createElement('div', {className: 'viewlevel'}, 'Magnification level: Sequence'),
       React.createElement('div', {className: 'genPreBtn sequenceMovingControlBtn glyphicon glyphicon-chevron-left', onClick: this.handleMoveBack }),
                React.createElement('span', {className: 'genPreBtn sequenceMovingControlBtn glyphicon glyphicon-chevron-right', onClick: this.handleMoveNext })
     )
   )
 );
},
renderSequenceLevel: function(){
   return (
     React.createElement('div', {className: 'inner_con_banner'},
        React.createElement('div', {id: 'sequence-panel'},
          // Variation bar
          React.createElement('div', {className: 'variation-bar-top variation-bar'},
            // Pointer bar top
            React.createElement('div', {className: 'variation-pointer-top'},
                this.renderSequencePointer(this.state.data)
            ) //----------variation-pointer-top----
          ),
          React.createElement('div', {style: {height: '35px'}}, ''

          ),
          React.createElement('div', {className: 'variation-bar-bottom variation-bar'},
            // Pointer bar top
            React.createElement('div', {className: 'variation-pointer-top'},
                this.renderSequencePointer(this.state.data)
            ) //----------variation-pointer-top----
          ),
            // Control pannel
            this.renderControlPanel()
        ) // --sequence-panel---
     ) // -----inner_con_banner---
   )
 },
 render: function() {
    return (
      React.createElement('div', {className: 'row'},
      React.createElement('div', {className: 'images_secs'}, this.renderSequenceLevel())
     )
    )
  }
});

var BannerGeneComponent = React.createClass({
    getInitialState: function () {
        return {
            geneSequence: {}
        }
    },
    componentWillMount(){
      this.handleBlockUI();
    },
    componentDidMount: function() {
       gene = this.getURLParameter('gene');
       this.handleLoadData(gene);
   },
    handleBlockUI: function() {
        var root = this;
        jQuery("#sequence-browser-gene").block({
               message: "<img src='/img/loading.gif' />",
               css: {
                   border: 'none',
                   backgroundColor: 'argb(255,255,255,255)',
                   marginTop: '50px'
               },
               fadeIn: 0,
               fadeOut: 0,
               overlayCSS:  {
                   backgroundColor: '#FFF',
                   opacity:         1,
                   cursor:          'wait'
               },
           });
    },
     getURLParameter:function(variable) {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
           var pair = vars[i].split("=");
           if(pair[0] == variable){return pair[1];}
       }
       return(false);
    },
    handleLoadData: function(gene){
        var _this = this;
        $.ajax({
             type: "get",
             dataType: 'json',
             crossDomain: true, //crossDomain = ?
             //url: 'http://api.sequence.dev.genopedia.com/gene/get/'+gene,
             //call to API with data input is gene
             url: 'http://sequence-viewer-front.genopedia.local/gene/get?gene=' + gene,
             data: '',
             async: false,
             success: function (result) {
                 if (result['status'] == 'ok') {
                   _this.setState({
                     geneSequence: result['data']['record']
                   });
                   jQuery("#sequence-browser-gene").unblock();

                 }
             },
             error: function (result) {
             }
         });
    },
    renderExonPointer: function (data) {
        var elements = [];
        var mapping = ['Short', 'Med', 'Long', 'Long', 'Med', 'Short'];

        ////////////////////////////////////////////////////////////////////////////
        var pointerIndicate = 0;
        var index = 0;
        var root = this;
        ///////////////////////////////////
        var start = Math.ceil(data.start / 20);
        var end = Math.ceil(data.end / 20);

        // exons
        pointer_exon = []
        if (data.exon) {
          for (var i = 0; i < data.exon.length; i++) {
              _start = Math.ceil(data.exon[i].start / 20);
              _end = Math.ceil(data.exon[i].end / 20);
              for (var j = _start; j <= _end; j++) {
                  pointer_exon.push(j)
              }
          }
        }

        // genes
        pointer_gene = []
        exonName = {}
        var eleTooltipParent = {}
        if (data.gene) {
          for (var i = 0; i < data.gene.length; i++) {
              _start = Math.ceil(data.gene[i].start / 20);
              _end = Math.ceil(data.gene[i].end / 20);
              for (var j = _start; j <= _end; j++) {
                  if ( jQuery.inArray( j, pointer_exon ) == -1 ) {
                      pointer_gene.push(j)
                  }
              }

              // exon name
              eN = 1;
              for (var e = 0; e < data.gene[i].exon.length; e++) {
                key = Math.ceil(data.gene[i].exon[e].start / 20);
                exonName[key] = 'Exon ' + eN;
                eN = eN + 1;
                var eleTooltip = [];
                eleTooltip.push(
                    React.createElement('p', {}, 'Chromosome: '+ data.chromosome),
                    React.createElement('p', {}, 'Position ['+ data.gene[i].exon[e].start+ "-"+ data.gene[i].exon[e].end+"]"),
                    React.createElement('p', {}, 'Gene: '+ data.gene[i].name),
                    React.createElement('p', {}, 'Num variation: '+ data.gene[i].exon[e].num_variation)
                );
                var eleTooltip = '';
                eleTooltip = 'Chromosome: '+ data.chromosome + "\n"+ 'Position ['+ data.gene[i].exon[e].start+ "-"+ data.gene[i].exon[e].end+"]" + "\n" + 'Gene: '+ data.gene[i].name+ "\n"+ 'Num variation: '+ data.gene[i].exon[e].num_variation;
                eleTooltipParent[key] = eleTooltip;
              }
          }
        }

        for (var i = start ; i <= end; i++) {
            var attrGeneWrapper = {};
            var attr = {};
            attr.className = 'exon-pointer pointer';
            attrGeneWrapper.className = 'gene-wrapper';

            if (jQuery.inArray( i, pointer_exon ) !== -1) {
                attrGeneWrapper.className += ' gene-wrapper-color';
                attr.className += ' icon-Gen'+mapping[pointerIndicate%6] + ' light';
                if (i in exonName) {
                  element = React.createElement('div', {},
                                React.createElement('div', {
                                    style: {'cursor': 'pointer'},
                                    className: 'labelName',
                                    'data-toggle': 'tooltip',
                                    'data-placement': 'bottom',
                                    'data-html': 'true',
                                    'title': eleTooltipParent[i]
                                    },
                                    exonName[i]
                                ),
                                React.createElement('div', attr)
                            )
                }else element = React.createElement('div', attr);


            }else if (jQuery.inArray( i, pointer_gene ) !== -1) {
                attrGeneWrapper.className += ' gene-wrapper-color';
                attr.className += ' icon-Gen'+mapping[pointerIndicate%6] + ' gray';
                element = React.createElement('div', attr);
            }else{
                attr.className += ' icon-Gen'+mapping[pointerIndicate%6] + ' gray';
                element = React.createElement('div', attr);
            }

            elements.push(
                React.createElement('div', {key: pointerIndicate, className: 'exon-sequence'},
                    React.createElement('div', attrGeneWrapper,
                        element
                    )
                )
              );
            pointerIndicate = pointerIndicate + 1;
        }
        ///////////////////////////////////
        return elements;
    },
    handleAjaxLoadExonSequence:function(pos) {

       if ( pos > 0 ) {
           var start = this.state.geneSequence.end + 1;
       }
       var chromosome= this.state.geneSequence.chromosome;
       var parent = this;

       var rs = [];
       var url = 'http://api.sequence.dev.genopedia.com/gene/load/'+chromosome+'/'+start;
       jQuery.ajax({
           type: "get",
           dataType: 'json',
           url: url,
           async: false,
           success: function (result) {
                if (result['status'] == 'ok') {
                    rs = result['data']['record'];
                }
           }
        });
        if (rs['start'] && rs['end']) {
            _current_state = this.state.geneSequence;
            var min = Math.min(_current_state.start, rs['start']);
            var max = Math.max(_current_state.end, rs['end']);
            _current_state.start = min;
            _current_state.end = max;
            this.setState({geneSequence: _current_state});
        }

    },
    renderGeneName: function(data){
      elements = [];
      stage = 0;
      k = 0;
      if (data.gene) {
        c_start = Math.ceil(data.start / 20);
        for (var i = 0; i < data.gene.length; i++) {
            _start = Math.ceil(data.gene[i].start / 20);
            margin = Math.ceil(((data.gene[i].start - data.start) *5))/ 20;
            margin_left = (margin - stage - k*30) + 'px';
            stage = margin;
            k = k + 1;
            elements.push(
              React.createElement('span', {className: 'topName', style: {'marginLeft': margin_left}}, data.gene[i].name)
            );
        }
      }
      return elements;
    },
    handleMoveNext: function() {
        var distance = $('.exon-sequence:last').offset().left - $('.exon-bar').offset().left - 450;
        if (distance <= $('.exon-bar').width()) {
            this.handleAjaxLoadExonSequence(1000);
        }
        var leftPos = $('.exon-bar').scrollLeft()+450;
        $(".exon-bar").stop().animate({scrollLeft: leftPos }, 400);
    },
    handleMoveBack: function() {
       var leftPos = $('.exon-bar').scrollLeft();
       $(".exon-bar").stop().animate({scrollLeft: leftPos - 450}, 400);
    },
    renderControlPanel: function () {
      return (
        React.createElement('div', {className: 'sequenceControlBtnGroup'},
          React.createElement('div', {className: 'rightGroup'},
            React.createElement('div', {className: 'viewlevel'}, 'Magnification level: Gene'),
            React.createElement('div', {className: 'genPreBtn sequenceMovingControlBtn glyphicon glyphicon-chevron-left', onClick: this.handleMoveBack }),
           React.createElement('div', {className: 'genPreBtn sequenceMovingControlBtn glyphicon glyphicon-chevron-right', onClick: this.handleMoveNext })
          )
        )
      );
    },
    render: function(){
        return (
            React.createElement('div', {className: 'row'},
               React.createElement('div', {className: 'inner_con_banner'},
                   React.createElement('div', {},
                       React.createElement('div', {className: 'exon-bar-top exon-bar'},
                           React.createElement('div', {className: 'sequenceGenesName'}, this.renderGeneName(this.state.geneSequence)),
                           React.createElement('div', {className: 'exon-pointer-top'},this.renderExonPointer(this.state.geneSequence))
                       ),
                       React.createElement('div', {style: {height: '15px'}}, ''

                       ),
                       React.createElement('div', {className: 'exon-bar-bottom exon-bar'},
                           React.createElement('div', {className: 'exon-pointer-top'}, this.renderExonPointer(this.state.geneSequence))
                       ),
                       this.renderControlPanel()
                   )
               )

           )

        );
    }
});