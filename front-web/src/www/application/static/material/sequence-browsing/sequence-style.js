import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "[class^=\"icon-\"]": {
        "fontFamily": "'icomoon' !important",
        "speak": "none",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontVariant": "normal",
        "textTransform": "none",
        "lineHeight": 1,
        "color": "#C3C3C3",
        "WebkitFontSmoothing": "antialiased",
        "MozOsxFontSmoothing": "grayscale"
    },
    "[class*=\" icon-\"]": {
        "fontFamily": "'icomoon' !important",
        "speak": "none",
        "fontStyle": "normal",
        "fontWeight": "normal",
        "fontVariant": "normal",
        "textTransform": "none",
        "lineHeight": 1,
        "color": "#C3C3C3",
        "WebkitFontSmoothing": "antialiased",
        "MozOsxFontSmoothing": "grayscale"
    },
    "icon-GenLong:before": {
        "content": "\\e900"
    },
    "icon-GenMed:before": {
        "content": "\\e901"
    },
    "icon-GenShort:before": {
        "content": "\\e902"
    },
    "sequence-browser": {
        "paddingTop": 50,
        "paddingRight": 0,
        "paddingBottom": 20,
        "paddingLeft": 20,
        "overflow": "hidden",
        "display": "block",
        "width": "100%"
    },
    "body": {
        "fontSize": 13,
        "color": "#333 !important"
    },
    "inner_con_banner": {
        "overflow": "hidden",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "sequenceControlBtnGroup": {
        "height": 23,
        "background": "#FFF",
        "bottom": "70%",
        "right": 6
    },
    "viewlevel": {
        "float": "left",
        "fontWeight": "800",
        "paddingTop": 3,
        "paddingRight": 15,
        "paddingBottom": 3,
        "paddingLeft": 15
    },
    "genPreBtn": {
        "paddingTop": 5,
        "paddingRight": 5,
        "paddingBottom": 5,
        "paddingLeft": 5
    },
    "sequenceMovingControlBtn": {
        "paddingTop": 12,
        "paddingRight": 12,
        "paddingBottom": 12,
        "paddingLeft": 12,
        "cursor": "pointer",
        "float": "left"
    },
    "gene-wrapper-color": {
        "background": "#d9f0ff"
    },
    "labelName": {
        "height": 33,
        "marginTop": -33,
        "fontWeight": "bold",
        "position": "absolute",
        "width": 60,
        "zIndex": 9,
        "color": "#1a3fa9"
    },
    "topName": {
        "overflow": "hidden",
        "fontSize": 15,
        "fontWeight": "900",
        "color": "#1a3fa9",
        "top": 77
    },
    "exon-bar-bottom": {
        "marginTop": -10,
        "overflow": "hidden"
    },
    "exon-bar-top": {
        "marginTop": 42,
        "overflow": "hidden"
    },
    "exon-pointer-top": {
        "width": 9999999
    },
    "exon-sequence": {
        "width": 5,
        "float": "left",
        "position": "relative"
    },
    "gene-wrapper": {
        "paddingTop": 42,
        "paddingRight": 0,
        "paddingBottom": 55,
        "paddingLeft": 0,
        "marginTop": 5
    },
    "exon-pointer": {
        "transform": "scale(1.25)"
    },
    "pointer": {
        "width": 27,
        "float": "left",
        "cursor": "pointer"
    },
    "gray": {
        "color": "gray"
    },
    "light": {
        "color": "#1A3FA9"
    },
    "sequence": {
        "width": 9,
        "float": "left"
    },
    "letter": {
        "fontWeight": "600",
        "fontFamily": "\"Courier New\", Courier, monospace",
        "height": 18
    },
    "snp-pointer": {
        "paddingTop": 8,
        "paddingRight": 9,
        "paddingBottom": 9,
        "paddingLeft": 0,
        "transform": "scale(2.30)"
    },
    "pointer-hightlight": {
        "color": "#1A3FA9 !important"
    },
    "pointer-hightlight-bonus": {
        "color": "green"
    },
    "letter-gray": {
        "marginTop": 1,
        "color": "#AEAEAE !important"
    },
    "snp-mutation-hightlight": {
        "color": "#FF5041 !important"
    },
    "snp-mutation-hightlight-bonus": {
        "color": "green !important"
    },
    "variation-pointer": {
        "cursor": "pointer"
    },
    "letter-mutation": {
        "color": "red"
    },
    "letter-mutation-bonus": {
        "color": "green"
    },
    "rightGroup": {
        "float": "right"
    },
    "images_secs": {
        "marginTop": 0,
        "position": "relative",
        "width": "100%",
        "display": "inline-block",
        "paddingTop": 0,
        "paddingRight": 10,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "variation-pointer-top": {
        "width": 999999
    },
    "variation-bar": {
        "height": 79,
        "paddingTop": 12,
        "overflow": "hidden"
    },
    "dropdown": {
        "position": "relative !important"
    },
    "dropdown-content": {
        "display": "none !important",
        "position": "fixed !important",
        "backgroundColor": "#f9f9f9 !important",
        "minWidth": "160px !important",
        "boxShadow": "0px 8px 16px 0px rgba(0,0,0,0.2) !important",
        "paddingTop": 12,
        "paddingRight": 16,
        "paddingBottom": "!important",
        "paddingLeft": 16,
        "zIndex": "1 !important"
    },
    "dropdown:hover dropdown-content": {
        "display": "inline !important"
    },
    "label-dropdown": {
        "fontWeight": "normal",
        "zIndex": 9,
        "color": "#000"
    }
});