from django.conf.urls import include, url

urlpatterns = [
    url(r'^',                     include('application.modules.integration_guide.urls')),
    url(r'^variation/',           include('application.modules.variation.urls')),
    url(r'^gene/',                include('application.modules.gene.urls')),
    url(r'^integration_guide/',   include('application.modules.integration_guide.urls')),
]
