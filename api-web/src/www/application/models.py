from __future__ import unicode_literals
from django.db import models
from . import constants

class Variation(models.Model):
    chromosome = models.CharField(max_length=225, default='')
    position = models.IntegerField()
    rsnumber = models.CharField(max_length=225,default='')
    gene     = models.CharField(max_length=225,)
    associated_disease = models.TextField()

class Gene(models.Model):
    start               = models.IntegerField()
    end                 = models.IntegerField()
    gene_id             = models.CharField(max_length=225)
    name                = models.CharField(max_length=225)
    description         = models.CharField(max_length=225)
    normal_function     = models.CharField(max_length=225)
    biotype             = models.CharField(max_length=225)
    havana_gene         = models.CharField(max_length=225)
    chromosome          = models.CharField(max_length=225)
    num_of_gene         = models.IntegerField(blank=True, null=True, default=0)
    num_of_disease      = models.IntegerField(blank=True, null=True, default=0)
    num_of_snp          = models.IntegerField(blank=True, null=True, default=0)
    is_reversed         = models.IntegerField(blank=True, null=True, default=0)

class Exon(models.Model):
    end = models.IntegerField()
    name = models.CharField(max_length=225)
    rank = models.IntegerField(blank=True, null=True, default=0)
    start = models.IntegerField()
    gene = models.CharField(max_length=225)
    id_gene=models.IntegerField()
    num_variation = models.IntegerField()
    chromosome = models.CharField(max_length=225)
