from django.conf.urls import include, url

urlpatterns = [
    url(r'^sequences/',                  include('application.modules.sequences.urls')),
    url(r'^gene/',                      include('application.modules.gene.urls')),
    url(r'^exon/',                      include('application.modules.exon.urls')),
]
