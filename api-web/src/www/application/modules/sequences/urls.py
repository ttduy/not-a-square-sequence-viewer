
from django.conf.urls import include, url
from . import handlers

urlpatterns = [
    url(r'^get$',    handlers.Get.as_view(),      name='sequences_get'),
    url(r'^load$',    handlers.Load.as_view(),      name='sequences_load'),
    url(r'^list$',    handlers.List.as_view(),      name='sequences_list'),
    url(r'^create$', handlers.Create.as_view(),   name='sequences_create'),
    url(r'^update$', handlers.Update.as_view(),   name='sequences_update'),
    url(r'^delete$', handlers.Delete.as_view(),   name='sequences_delete'),
    url(r'^getid$', handlers.GetId.as_view(),   name='sequences_getid'),
    url(r'^bulk_create$', handlers.BulkCreate.as_view(), name='sequences_bulk_create'),
]
