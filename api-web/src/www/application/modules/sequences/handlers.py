
import gzip

from notasquare.urad_api import *
from application.models import *
from application.settings import dev as settings
from . import components

class List(handlers.standard.ListHandler):
    def create_query(self, data):
        query = Variation.objects
        if 'chromosome' in data:
            query = query.filter(chromosome__contains=data['chromosome'])
        if 'position' in data:
            query = query.filter(potision=data['position'])
        if 'snp' in data:
            query = query.filter(rsnumber__contains=data['snp'])
        return query
    def serialize_entry(self, variation):
        return {
            'id':               variation.id,
            'chromosome':       variation.chromosome,
            'position':         variation.position,
            'rsnumber':         variation.rsnumber,
            'gene':             variation.gene
        }

class Create(handlers.standard.CreateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if not parser.parse('chromosome', 'string'):
            self.add_error('chromosome', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('position', 'integer'):
            self.add_error('position', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('rsnumber', 'string'):
            self.add_error('rsnumber', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def create(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        variation = Variation()
        variation.chromosome = data['chromosome']
        variation.rsnumber = data['rsnumber']

        if is_number(data['position']):
            variation.position = data['position']
        else:
            variation.position = 0
        variation.associated_disease = data['associated_disease']
        variation.gene = data['gene']
        variation.save()
        return variation

class Update(handlers.standard.UpdateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if 'chromosome' in params:
            if not parser.parse('chromosome', 'string'):
                self.add_error('chromosome', 'MUST_NOT_BE_EMPTY')
        if 'position' in params:
            if not parser.parse('position', 'integer'):
                self.add_error('position', 'MUST_NOT_BE_EMPTY')
        if 'rsnumber' in params:
            if not parser.parse('rsnumber', 'string'):
                self.add_error('rsnumber', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def update(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        variation = Variation.objects.get(pk=data['id'])
        if 'chromosome' in data:
            variation.chromosome = data['chromosome']
        if 'rsnumber' in data:
            variation.rsnumber = data['rsnumber']
        if is_number(data['position']):
            variation.position = data['position']
        else:
            variation.position = 0

        variation.associated_disease = data['associated_disease']
        variation.gene = data['gene']
        variation.save()
        return variation

class Delete(handlers.standard.DeleteHandler):
    def delete(self, data):
        variation = Variation.objects.get(pk=data['id'])
        variation.delete()
        return 1

class GetId(handlers.standard.GetHandler):
    def get_data(self, data):
        variation = Variation.objects.get(pk=data['id'])
        return {
            'id':               variation.id,
            'chromosome':       variation.chromosome,
            'position':         variation.position,
            'rsnumber':         variation.rsnumber,
            'gene':             variation.gene,
            'associated_disease': variation.associated_disease
        }

class Get(handlers.standard.GetHandler):
    def resolve_offset(self, pos):
        num_lines = int(pos / 60)
        line_offset = pos % 60
        return (num_lines * 61) + line_offset - 1

    def get_data(self, data):
        variation = Variation.objects.get(rsnumber=data['rsnumber'])
        start = int(variation.position) - int(30)
        end = int(variation.position) + int(1000)		#===> use 1000 instead 200
        file = settings.FASTSA[str(variation.chromosome)]
        with gzip.open(file) as file:
            base_offset = 0
            for line in file:
                base_offset = len(line)
                break

            start_offset = base_offset + self.resolve_offset(start)
            end_offset = base_offset + self.resolve_offset(end)
            if start_offset > end_offset:
                return ''

            sequence = ''
            file.seek(start_offset, 0)
            for i in range(start_offset, end_offset):
                c = file.read(1)
                if c == '\n':
                    continue
                sequence += c
            return {
                'parameters': {'chromosome': variation.chromosome,'start': start, 'end': end, 'position':variation.position, 'rsnumber': data['rsnumber']},
                'variation': components.SentenceHelper().load_variation_db(start = start, end=end),
                'sequence': sequence,
            }
        return {}

class Load(handlers.standard.GetHandler):
    def resolve_offset(self, pos):
        num_lines = int(pos / 60)
        line_offset = pos % 60
        return (num_lines * 61) + line_offset - 1

    def get_data(self, data):
        variation = Variation.objects.get(rsnumber=data['rsnumber'])
        start = int(data['start'])
        end = int(data['end'])
        file = settings.FASTSA[str(variation.chromosome)]
        with gzip.open(file) as file:
            base_offset = 0
            for line in file:
                base_offset = len(line)
                break

            start_offset = base_offset + self.resolve_offset(start)
            end_offset = base_offset + self.resolve_offset(end)
            if start_offset > end_offset:
                return ''

            sequence = ''
            file.seek(start_offset, 0)
            for i in range(start_offset, end_offset):
                c = file.read(1)
                if c == '\n':
                    continue
                sequence += c
            return {
                'parameters': {'chromosome': variation.chromosome,'start': start, 'end': end, 'position':variation.position, 'rsnumber': data['rsnumber']},
                'variation': components.SentenceHelper().load_variation_db(start = start, end=end),
                'sequence': sequence,
            }
        return ''

# hack, workaround for demo
class BulkCreate(handlers.standard.POSTHandler):
    def POST(self, params):
        for data in params['variations']:
            variation = Variation()
            variation.chromosome = data.get('chromosome', '')
            variation.position = int(data.get('position', 0))
            variation.rsnumber = data.get('rsnumber', '')
            variation.gene = data.get('gene', '')
            variation.associated_disease = data.get('associated_disease', '')
            variations.append(variation)
        Variation.objects.bulk_create(variations)
        return ('ok', {})
