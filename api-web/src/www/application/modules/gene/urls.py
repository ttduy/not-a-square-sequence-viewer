
from django.conf.urls import include, url
from . import handlers

urlpatterns = [
    url(r'^get$',    handlers.Get.as_view(),      name='gene_get'),
    url(r'^load/(?P<chromosome>[-\w]+)/(?P<start>[-\w]+)$',    handlers.Load.as_view(),      name='gene_load'),
    url(r'^list$',    handlers.List.as_view(),      name='gene_list'),
    url(r'^create$', handlers.Create.as_view(),   name='gene_create'),
    url(r'^update$', handlers.Update.as_view(),   name='gene_update'),
    url(r'^delete$', handlers.Delete.as_view(),   name='gene_delete'),
    url(r'^getid$', handlers.GetId.as_view(),   name='gene_getid'),

]
