
from notasquare.urad_api import *
from application.models import *
from application.settings import dev as settings
from . import components

class List(handlers.standard.ListHandler):
    def create_query(self, data):
        query = Gene.objects
        if 'name' in data:
            query = query.filter(name__contains=data['name'])
        return query
    def serialize_entry(self, gene):
        return {
            'id':               gene.id,
            'name':       gene.name,
            'start':         gene.start,
            'end':         gene.end,
            'chromosome':   gene.chromosome
        }

class Create(handlers.standard.CreateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if not parser.parse('name', 'string'):
            self.add_error('name', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('end', 'integer'):
            self.add_error('end', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('start', 'integer'):
            self.add_error('start', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('gene_id', 'string'):
            self.add_error('gene_id', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def create(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        gene = Gene()
        gene.name               = data['name']
        gene.start              = data['start']
        gene.end                = data['end']
        gene.description        = data['description']
        gene.normal_function    = data['normal_function']
        gene.chromosome         = data['chromosome']
        gene.gene_id            = data['gene_id']
        gene.biotype            = data['biotype']
        gene.havana_gene        = data['havana_gene']

        if is_number(data['num_of_gene']):
            gene.num_of_gene        = data['num_of_gene']
        if is_number(data['num_of_disease']):
            gene.num_of_gene        = data['num_of_disease']
        if is_number(data['is_reversed']):
            gene.num_of_gene        = data['is_reversed']
        if is_number(data['num_of_snp']):
            gene.num_of_gene        = data['num_of_snp']

        gene.save()
        return gene

class Update(handlers.standard.UpdateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if 'name' in params:
            if not parser.parse('name', 'string'):
                self.add_error('name', 'MUST_NOT_BE_EMPTY')
        if 'end' in params:
            if not parser.parse('end', 'integer'):
                self.add_error('end', 'MUST_NOT_BE_EMPTY')
        if 'start' in params:
            if not parser.parse('start', 'integer'):
                self.add_error('start', 'MUST_NOT_BE_EMPTY')
        if 'chromosome' in params:
            if not parser.parse('chromosome', 'integer'):
                self.add_error('chromosome', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def update(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        gene = Gene.objects.get(pk=data['id'])
        gene.name               = data['name']
        gene.start              = data['start']
        gene.end                = data['end']
        gene.description        = data['description']
        gene.normal_function    = data['normal_function']
        gene.chromosome         = data['chromosome']
        gene.gene_id            = data['gene_id']
        gene.biotype            = data['biotype']
        gene.havana_gene        = data['havana_gene']

        if is_number(data['num_of_gene']):
            gene.num_of_gene        = data['num_of_gene']
        if is_number(data['num_of_disease']):
            gene.num_of_gene        = data['num_of_disease']
        if is_number(data['is_reversed']):
            gene.num_of_gene        = data['is_reversed']
        if is_number(data['num_of_snp']):
            gene.num_of_gene        = data['num_of_snp']
        gene.save()
        return gene

class Delete(handlers.standard.DeleteHandler):
    def delete(self, data):
        gene = Gene.objects.get(pk=data['id'])
        gene.delete()
        return 1

class GetId(handlers.standard.GetHandler):
    def get_data(self, data):
        gene = Gene.objects.get(pk=data['id'])
        return {
            'id':                   gene.id,
            'name':                 gene.name,
            'start':                gene.start,
            'end':                  gene.end,
            'chromosome':           gene.chromosome,
            'gene_id':              gene.gene_id,
            'num_of_snp':           gene.num_of_snp,
            'num_of_gene':          gene.num_of_gene,
            'num_of_disease':       gene.num_of_disease,
            'description':          gene.description,
            'normal_function':      gene.normal_function,
            'is_reversed':          gene.is_reversed,
            'havana_gene':          gene.havana_gene,
            'biotype':              gene.biotype    
        }

class Get(handlers.standard.GetHandler):
    def get_data(self, data):
        gene = Gene.objects.get(name=data['gene'])
        new_start = int(gene.start) - 100
        new_end = int(gene.end) + 20000
        other_gene = Gene.objects.filter(chromosome=gene.chromosome, start__gte=new_start, start__lte=new_end).order_by('start')
        genes = []
        exons = []
        for item in other_gene:
            exon = Exon.objects.filter(gene=item.name)
            for ex in exon:
                exons.append({
                    'name': ex.name,
                    'start': ex.start,
                    'end': ex.end,
                    'gene': ex.gene,
                    'num_variation': ex.num_variation
                })

            genes.append({
                'id': item.id,
                'name': item.name,
                'start': item.start,
                'end': item.end,
                'chromosome': item.chromosome,
                'exon': components.GeneHelper().load_exon_db(item.name)
            })
        return {
            'id':             gene.id,
            'name':           gene.name,
            'start':          new_start,
            'end':            new_end,
            'chromosome':     gene.chromosome,
            'gene':           genes,
            'exon':           exons

        }

class Load(handlers.standard.GetHandler):
    def get_data(self, data):
        end = int(data['start']) + 20000
        gene = Gene.objects.filter(chromosome=data['chromosome'], start__gte=data['start'], start__lte=end)
        genes = []
        exons = []
        if gene:
            for item in gene:
                exon = Exon.objects.filter(gene=item.name)
                for ex in exon:
                    exons.append({
                        'name': ex.name,
                        'start': ex.start,
                        'end': ex.end,
                        'gene': ex.gene,
                        'num_variation': ex.num_variation
                    })

                genes.append({
                    'name': item.name,
                    'start': item.start,
                    'end': item.end,
                    'chromosome': item.chromosome,
                    'exon': components.GeneHelper().load_exon_db(item.name)
                })
            return {
                'name':           gene.name,
                'start':          new_start,
                'end':            new_end,
                'chromosome':     gene.chromosome,
                'gene':           genes,
                'exon':           exons
            }
        else:
            return ''
