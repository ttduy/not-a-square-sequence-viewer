
from django.conf.urls import include, url
from . import handlers

urlpatterns = [
    url(r'^list$',    handlers.List.as_view(),      name='exon_list'),
    url(r'^create$', handlers.Create.as_view(),   name='exon_create'),
    url(r'^update$', handlers.Update.as_view(),   name='exon_update'),
    url(r'^delete$', handlers.Delete.as_view(),   name='exon_delete'),
    url(r'^getid$', handlers.GetId.as_view(),   name='exon_getid'),
]
