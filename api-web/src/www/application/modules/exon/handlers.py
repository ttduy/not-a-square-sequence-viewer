
from notasquare.urad_api import *
from application.models import *
from application.settings import dev as settings
from . import components

class List(handlers.standard.ListHandler):
    def create_query(self, data):
        query = Exon.objects
        if 'name' in data:
            query = query.filter(name__contains=data['name'])
        if 'gene' in data:
            query = query.filter(id_gene__contains=data['gene'])
        return query
    def serialize_entry(self, exon):
        return {
            'id':               exon.id,
            'name':             exon.name,
            'start':            exon.start,
            'end':              exon.end,
            'rank':             exon.rank,
            'num_variation':    exon.num_variation,
            'gene':             exon.gene,
            'id_gene':          exon.id_gene,
            'chromosome':       exon.chromosome
        }

class Create(handlers.standard.CreateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if not parser.parse('name', 'string'):
            self.add_error('name', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('end', 'integer'):
            self.add_error('end', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('start', 'integer'):
            self.add_error('start', 'MUST_NOT_BE_EMPTY')
        if not parser.parse('num_variation', 'integer'):
            self.add_error('num_variation', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def create(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        exon = Exon()
        exon.name = data['name']
        exon.start = data['start']
        exon.end = data['end']
        exon.num_variation = data['num_variation']
        if is_number(data['rank']):
            exon.rank = data['rank']
        gene_id = Gene.objects.get(pk=data['gene'])
        exon.gene = gene_id.name
        exon.chromosome = gene_id.chromosome
        exon.id_gene = gene_id.id
        exon.save()
        return exon

class Update(handlers.standard.UpdateHandler):
    def parse_and_validate(self, params):
        parser = self.container.create_parser(params)
        if 'name' in params:
            if not parser.parse('name', 'string'):
                self.add_error('name', 'MUST_NOT_BE_EMPTY')
        if 'end' in params:
            if not parser.parse('end', 'integer'):
                self.add_error('end', 'MUST_NOT_BE_EMPTY')
        if 'start' in params:
            if not parser.parse('start', 'integer'):
                self.add_error('start', 'MUST_NOT_BE_EMPTY')
        return parser.get_data()
    def update(self, data):
        def is_number(var):
            try:
                if var == int(var):
                    return True
            except Exception:
                return False
        exon = Exon.objects.get(pk=data['id'])
        if 'name' in data:
            exon.name = data['name']
        if 'start' in data:
            exon.start = data['start']
        if 'end' in data:
            exon.end = data['end']
        if 'rank' in data:
            exon.rank = data['rank']
        if 'gene' in data:
            gene_id = Gene.objects.get(pk=data['gene'])
            exon.gene = gene_id.name
            exon.chromosome = gene_id.chromosome
        exon.num_variation = data['num_variation']
        exon.save()
        return exon

class Delete(handlers.standard.DeleteHandler):
    def delete(self, data):
        exon = Exon.objects.get(pk=data['id'])
        exon.delete()
        return 1

class GetId(handlers.standard.GetHandler):
    def get_data(self, data):
        exon = Exon.objects.get(pk=data['id'])
        return {
            'id':               exon.id,
            'name':             exon.name,
            'start':            exon.start,
            'end':              exon.end,
            'rank':             exon.rank,
            'num_variation':    exon.num_variation,
            'gene':             exon.gene,
            'id_gene':          exon.id_gene,
            'chromosome':       exon.chromosome
        }
