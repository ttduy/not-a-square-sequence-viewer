from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^gene/',                      include('application.modules.gene.urls')),
    url(r'^variation/',                      include('application.modules.variation.urls')),
    url(r'^exon/',                      include('application.modules.exon.urls')),
    url(r'^$',                             actions.Home.as_view(), name='home'),
]
