
from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^list$',                    actions.List.as_view(),     name='gene_list'),
    url(r'^create$',                  actions.Create.as_view(),   name='gene_create'),
    url(r'^delete/(?P<id>([A-Za-z0-9:]+))$', actions.Delete.as_view(),   name='gene_delete'),
    url(r'^update/(?P<page_id>([A-Za-z0-9:]+))$',   actions.Update.as_view(),   name='gene_update'),
]
