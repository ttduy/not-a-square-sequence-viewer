from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from notasquare.urad_web import actions, page_contexts, widgets
from notasquare.urad_web_material import renderers
from application import constants
from . import components

class List(actions.crud.ListAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    class TableRenderer(renderers.widgets.table.DataTableRenderer):
        def render_cell_actions(self, table, row):
            html  = '<div class="btn-group btn-group">'
            html += '    <a class="btn btn-xs btn-primary" href="/gene/update/%s">Edit</a>' % (row['id'])
            html += '    <a class="btn btn-xs btn-danger" href="/gene/delete/%s" onclick="return confirm(\'Are you really want to delete this?\')">Delete</a>'  % (row['id'])
            html += '</div>'
            return html
    def create_table(self):
        table = widgets.table.DataTable()
        table.set_title('Gene')
        table.set_subtitle('List of genes')
        table.create_button('create', '/gene/create', 'zmdi-plus')
        table.create_column('id', 'ID', '30%', sortable=True)
        table.create_column('name', 'Name', '20%')
        table.create_column('start', 'Start', '10%')
        table.create_column('end', 'End', '10%')
        table.create_column('chromosome', 'Chromosome', '10%')
        table.create_column('actions', '', '14%')
        table.add_field(widgets.field.Textbox('name'))
        table.renderer = self.TableRenderer()
        table.renderer.table_form_renderer = renderers.widgets.form.TableFormRenderer()
        table.renderer.table_form_renderer.add_field('name', 'Name', colspan=8)
        table.renderer.table_form_renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        return table
    def load_table_data(self, table_form_data, sortkey, sortdir, page_number):
        return components.PageStore(self.get_container()).list(table_form_data, sortkey, sortdir, page_number)

class Create(actions.crud.CreateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Gene')
        form.add_field(widgets.field.Textbox('name'))
        form.add_field(widgets.field.Textbox('start'))
        form.add_field(widgets.field.Textbox('end'))
        form.add_field(widgets.field.Textbox('gene_id'))
        form.add_field(widgets.field.Textbox('chromosome'))
        form.add_field(widgets.field.Textbox('description'))
        form.add_field(widgets.field.Textbox('num_of_gene'))
        form.add_field(widgets.field.Textbox('num_of_disease'))
        form.add_field(widgets.field.Textbox('num_of_snp'))
        form.add_field(widgets.field.Textbox('normal_function'))
        form.add_field(widgets.field.Textbox('biotype'))
        form.add_field(widgets.field.Textbox('havana_gene'))
        form.add_field(widgets.field.Textbox('is_reversed'))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        # form.renderer.add_section('gene')
        form.renderer.add_field('name', 'Name')
        form.renderer.add_field('start', 'Start')
        form.renderer.add_field('end', 'End')
        form.renderer.add_field('gene_id', 'Gene_id')
        form.renderer.add_field('chromosome', 'Chromosome')
        form.renderer.add_field('description', 'Description')
        form.renderer.add_field('num_of_gene', 'Num_of_gene')
        form.renderer.add_field('num_of_disease', 'Num_of_disease')
        form.renderer.add_field('num_of_snp', 'Num_of_snp')
        form.renderer.add_field('normal_function', 'Normal_function')
        form.renderer.add_field('biotype', 'Biotype')
        form.renderer.add_field('havana_gene', 'Havana_gene')
        form.renderer.add_field('is_reversed', 'Is_reversed')
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        return form
    def load_form(self, form):
        form.set_form_data({
        })
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).create(data)


class Update(actions.crud.UpdateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Gene')
        form.add_field(widgets.field.Textbox('name'))
        form.add_field(widgets.field.Textbox('start'))
        form.add_field(widgets.field.Textbox('end'))
        form.add_field(widgets.field.Textbox('num_of_gene'))
        form.add_field(widgets.field.Textbox('description'))
        form.add_field(widgets.field.Textbox('num_of_disease'))
        form.add_field(widgets.field.Textbox('normal_function'))
        form.add_field(widgets.field.Textbox('gene_id'))
        form.add_field(widgets.field.Textbox('biotype'))
        form.add_field(widgets.field.Textbox('num_of_snp'))
        form.add_field(widgets.field.Textbox('havana_gene'))
        form.add_field(widgets.field.Textbox('is_reversed'))
        form.add_field(widgets.field.Textbox('chromosome'))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        # form.renderer.add_section('gene')
        form.renderer.add_field('name', 'Name')
        form.renderer.add_field('start', 'Start')
        form.renderer.add_field('end', 'End')
        form.renderer.add_field('gene_id', 'Gene_id')
        form.renderer.add_field('chromosome', 'Chromosome')
        form.renderer.add_field('description', 'Description')
        form.renderer.add_field('num_of_gene', 'Num_of_gene')
        form.renderer.add_field('num_of_disease', 'Num_of_disease')
        form.renderer.add_field('num_of_snp', 'Num_of_snp')
        form.renderer.add_field('normal_function', 'Normal_function')
        form.renderer.add_field('biotype', 'Biotype')
        form.renderer.add_field('havana_gene', 'Havana_gene')
        form.renderer.add_field('is_reversed', 'Is_reversed')
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        return form
    def load_form(self, form):
        result = components.PageStore(self.get_container()).get(self.params['page_id'])
        if result['status'] == 'ok':
            record = result['data']['record']
            form.set_form_data({
                'name':       record['name'],
                'start':      record['start'],
                'end':       record['end'],
                'chromosome':      record['chromosome'],
                'gene_id': record['gene_id'],
                'num_of_snp':           record['num_of_snp'],
                'num_of_gene':          record['num_of_gene'],
                'num_of_disease':       record['num_of_disease'],
                'description':          record['description'],
                'normal_function':      record['normal_function'],
                'is_reversed':          record['is_reversed'],
                'havana_gene':          record['havana_gene'],
                'biotype':              record['biotype']
            })
        else:
            form.add_message('danger', "Can't load form")
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).update(data, self.params['page_id'])


class Delete(actions.crud.DeleteAction):
    def GET(self):
        result = components.PageStore(self.get_container()).delete(self.params['id'])
        return HttpResponseRedirect('/gene/list')
