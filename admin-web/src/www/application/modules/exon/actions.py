from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from notasquare.urad_web import actions, page_contexts, widgets
from notasquare.urad_web_material import renderers
from application import constants
from . import components
from application.modules.gene import components as gene_component

class List(actions.crud.ListAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    class TableRenderer(renderers.widgets.table.DataTableRenderer):
        def render_cell_actions(self, table, row):
            html  = '<div class="btn-group btn-group">'
            html += '    <a class="btn btn-xs btn-primary" href="/exon/update/%s">Edit</a>' % (row['id'])
            html += '    <a class="btn btn-xs btn-danger" href="/exon/delete/%s" onclick="return confirm(\'Are you really want to delete this?\')">Delete</a>'  % (row['id'])
            html += '</div>'
            return html
    def create_table(self):
        table = widgets.table.DataTable()
        table.set_title('Exon')
        table.set_subtitle('List of exons')
        table.create_button('create', '/exon/create', 'zmdi-plus')
        table.create_column('id', 'ID', '5%', sortable=True)
        table.create_column('name', 'Name', '15%')
        table.create_column('start', 'Start', '10%')
        table.create_column('end', 'End', '10%')
        table.create_column('num_variation', 'Num variation', '10%')
        table.create_column('gene', 'Gene', '10%')
        table.create_column('actions', '', '14%')
        table.add_field(widgets.field.Textbox('name'))
        table.add_field(widgets.field.Textbox('gene'))
        table.renderer = self.TableRenderer()
        table.renderer.table_form_renderer = renderers.widgets.form.TableFormRenderer()
        table.renderer.table_form_renderer.add_field('name', 'Name', colspan=4)
        table.renderer.table_form_renderer.add_field('gene', 'Gene', colspan=4)
        table.renderer.table_form_renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        return table
    def load_table_data(self, table_form_data, sortkey, sortdir, page_number):
        return components.PageStore(self.get_container()).list(table_form_data, sortkey, sortdir, page_number)

class Create(actions.crud.CreateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Exon')
        form.add_field(widgets.field.Textbox('name'))
        form.add_field(widgets.field.Textbox('start'))
        form.add_field(widgets.field.Textbox('end'))
        form.add_field(widgets.field.Textbox('rank'))
        form.add_field(widgets.field.Textbox('num_variation'))
        gene_combobox = gene_component.PageStore(self.get_container()).populate_combobox()
        form.add_field(widgets.field.Combobox('gene', choices=gene_combobox))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        # form.renderer.add_section('exon')
        form.renderer.add_field('name', 'Name')
        form.renderer.add_field('start', 'Start')
        form.renderer.add_field('end', 'End')
        form.renderer.add_field('num_variation', 'Num variation')
        form.renderer.add_field('rank', 'Rank')
        form.renderer.add_field('gene', 'Gene')
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        form.renderer.set_field_renderer('combobox', renderers.widgets.field.ComboboxRenderer())
        return form
    def load_form(self, form):
        form.set_form_data({
        })
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).create(data)


class Update(actions.crud.UpdateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Exon')

        form.add_field(widgets.field.Textbox('name'))
        form.add_field(widgets.field.Textbox('start'))
        form.add_field(widgets.field.Textbox('end'))
        form.add_field(widgets.field.Textbox('num_variation'))
        form.add_field(widgets.field.Textbox('rank'))
        gene_combobox = gene_component.PageStore(self.get_container()).populate_combobox()
        form.add_field(widgets.field.Combobox('gene', choices=gene_combobox))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        # form.renderer.add_section('exon')

        form.renderer.add_field('name', 'Name')
        form.renderer.add_field('start', 'Start')
        form.renderer.add_field('end', 'End')
        form.renderer.add_field('num_variation', 'Num variation')
        form.renderer.add_field('rank', 'Rank')
        form.renderer.add_field('gene', 'Gene')
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        form.renderer.set_field_renderer('combobox', renderers.widgets.field.ComboboxRenderer())
        return form
    def load_form(self, form):
        result = components.PageStore(self.get_container()).get(self.params['page_id'])
        if result['status'] == 'ok':
            record = result['data']['record']
            form.set_form_data({
                'name':       record['name'],
                'start':      record['start'],
                'end':              record['end'],
                'num_variation':    record['num_variation']  ,
                'rank':      record['rank'],
                'gene':     record['id_gene']
            })
        else:
            form.add_message('danger', "Can't load form")
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).update(data, self.params['page_id'])


class Delete(actions.crud.DeleteAction):
    def GET(self):
        result = components.PageStore(self.get_container()).delete(self.params['id'])
        return HttpResponseRedirect('/exon/list')
