from django.conf import settings
from application.modules.common import page_contexts
import json
class PageStore(object):
    def __init__(self, container):
        self.container = container
    def list(self, params={}, sortkey='id', sortdir='desc', page_number=1):
        params['_page_start'] = (page_number - 1) * 10
        params['_page_num'] = 10
        params['_sort_key'] = sortkey
        params['_sort_dir'] = sortdir
        data = self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/exon/list')
        return data['data']

    def get(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/exon/getid', GET={'id': id})
    def create(self, data):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/exon/create', POST=data)
    def update(self, data, id):
        data['id'] = id
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/exon/update', POST=data)
    def delete(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/exon/delete', POST={'id': id})


class FullPageContext(page_contexts.FullPageContext):
    def __init__(self, params, container):
        super(FullPageContext, self).__init__(container.request)
        if 'page_id' in params:
            self.submenu.create_menu_group('update', 'Update', '/exon/update/%s' % (str(params['page_id'])), 'zmdi-border-all')
            # self.submenu.create_menu_group('block', 'Block', '/exon/detail/%s/block/list' % (str(params['exon_id'])), 'zmdi-border-all')
            # self.submenu.create_menu_group('comment', 'Comment', '/exon/detail/%s/comment/list' % (str(params['exon_id'])), 'zmdi-border-all')
