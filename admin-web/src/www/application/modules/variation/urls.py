
from django.conf.urls import include, url
from . import actions

urlpatterns = [
    url(r'^list$',                    actions.List.as_view(),     name='page_list'),
    url(r'^create$',                  actions.Create.as_view(),   name='page_create'),
    url(r'^delete/(?P<id>([0-9]+))$', actions.Delete.as_view(),   name='page_delete'),
    url(r'^update/(?P<page_id>([0-9]+))$',   actions.Update.as_view(),   name='page_update'),
    
    # url(r'^detail/(?P<page_id>([0-9]+))/block/',    include('application.modules.page.block.urls')),    
    # url(r'^detail/(?P<page_id>([0-9]+))/comment/',    include('application.modules.page.comment.urls')),    
]