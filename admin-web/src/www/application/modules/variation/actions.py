from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from notasquare.urad_web import actions, page_contexts, widgets
from notasquare.urad_web_material import renderers
from application import constants
from . import components

class List(actions.crud.ListAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    class TableRenderer(renderers.widgets.table.DataTableRenderer):
        def render_cell_actions(self, table, row):
            html  = '<div class="btn-group btn-group">'
            html += '    <a class="btn btn-xs btn-primary" href="/variation/update/%s">Edit</a>' % (row['id'])
            html += '    <a class="btn btn-xs btn-danger" href="/variation/delete/%s" onclick="return confirm(\'Are you really want to delete this?\')">Delete</a>'  % (row['id'])
            html += '</div>'
            return html
    def create_table(self):
        table = widgets.table.DataTable()
        table.set_title('Variation')
        table.set_subtitle('List of variation')
        table.create_button('create', '/variation/create', 'zmdi-plus')
        table.create_column('id', 'ID', '6%', sortable=True)
        table.create_column('chromosome', 'Chromosome', '20%')
        table.create_column('position', 'Position', '20%')
        table.create_column('rsnumber', 'Rsnumber', '20%')
        table.create_column('gene', 'Gene', '20%')
        table.create_column('actions', '', '14%')
        table.add_field(widgets.field.Textbox('chromosome'))
        table.add_field(widgets.field.Textbox('position'))
        table.add_field(widgets.field.Textbox('rsnumber'))
        table.add_field(widgets.field.Textbox('gene'))

        table.renderer = self.TableRenderer()
        table.renderer.table_form_renderer = renderers.widgets.form.TableFormRenderer()
        table.renderer.table_form_renderer.add_field('chromosome', 'Chromosome', colspan=3)
        table.renderer.table_form_renderer.add_field('position', 'Position', colspan=3)
        table.renderer.table_form_renderer.add_field('rsnumber', 'Rsnumber', colspan=3)
        table.renderer.table_form_renderer.add_field('gene', 'Gene', colspan=3)
        table.renderer.table_form_renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        return table
    def load_table_data(self, table_form_data, sortkey, sortdir, page_number):
        #goi API o day!!!
        return components.PageStore(self.get_container()).list(table_form_data, sortkey, sortdir, page_number)


class Create(actions.crud.CreateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Variation')
        form.add_field(widgets.field.Textbox('chromosome'))
        form.add_field(widgets.field.Textbox('position'))
        form.add_field(widgets.field.Textbox('rsnumber'))
        form.add_field(widgets.field.Textbox('gene'))
        form.add_field(widgets.field.Textarea('associated_disease'))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        # form.renderer.add_section('Variation')
        form.renderer.add_field('chromosome', 'Chromosome')
        form.renderer.add_field('position', 'Position')
        form.renderer.add_field('rsnumber', 'Rsnumber')
        form.renderer.add_field('gene', 'Gene')
        form.renderer.add_field('associated_disease', 'Associated disease', rows=5)
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        form.renderer.set_field_renderer('textarea', renderers.widgets.field.TextareaRenderer())
        return form
    def load_form(self, form):
        form.set_form_data({
        })
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).create(data)


class Update(actions.crud.UpdateAction):
    def create_page_context(self):
        return components.FullPageContext(self.params, self.container)
    def create_form(self):
        form = widgets.form.Form()
        form.set_title('Page')
        form.add_field(widgets.field.Textbox('chromosome'))
        form.add_field(widgets.field.Textbox('position'))
        form.add_field(widgets.field.Textbox('rsnumber'))
        form.add_field(widgets.field.Textbox('gene'))
        form.add_field(widgets.field.Textarea('associated_disease'))
        form.renderer = renderers.widgets.form.HorizontalFormRenderer()
        form.renderer.add_field('chromosome', 'Chromosome')
        form.renderer.add_field('position', 'Position')
        form.renderer.add_field('rsnumber', 'Rsnumber')
        form.renderer.add_field('gene', 'Gene')
        form.renderer.add_field('associated_disease', 'Associated disease', rows=5)
        form.renderer.set_field_renderer('textbox', renderers.widgets.field.TextboxRenderer())
        form.renderer.set_field_renderer('textarea', renderers.widgets.field.TextareaRenderer())
        return form
    def load_form(self, form):
        result = components.PageStore(self.get_container()).get(self.params['page_id'])
        if result['status'] == 'ok':
            record = result['data']['record']
            form.set_form_data({
                'chromosome':   record['chromosome'],
                'position':     record['position'],
                'rsnumber':     record['rsnumber'],
                'gene':         record['gene'],
                'associated_disease': record['associated_disease']
            })
        else:
            form.add_message('danger', "Can't load form")
    def process_form_data(self, data):
        return components.PageStore(self.get_container()).update(data, self.params['page_id'])


class Delete(actions.crud.DeleteAction):
    def GET(self):
        result = components.PageStore(self.get_container()).delete(self.params['id'])
        return HttpResponseRedirect('/variation/list')
