from django.conf import settings
from application.modules.common import page_contexts

class PageStore(object):
    def __init__(self, container):
        self.container = container
    def list(self, params={}, sortkey='id', sortdir='desc', page_number=1):
        params['_pager_start'] = (page_number - 1) * 10
        params['_pager_num'] = 10
        params['_sort_key'] = sortkey
        params['_sort_dir'] = sortdir
        data = self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/list', GET=params)
        print 'data: ', data
        return data['data']
    def get(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/getid', GET={'id': id})
    def create(self, data):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/create', POST=data)
    def update(self, data, id):
        data['id'] = id
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/update', POST=data)
    def delete(self, id):
        return self.container.call_api(settings.SEQUENCE_VIEWER_API_URL + '/sequences/delete', POST={'id': id})
    def populate_combobox(self, kind=''):
        choices = []
        params = {}
        if kind != '':
            params['kind'] = kind
        records = self.list(sortkey='title', sortdir='asc', params=params)
        for record in records['records']:
            choices.append({
                'id':     record['id'],
                'label':  record['name']
            })
        return choices

class FullPageContext(page_contexts.FullPageContext):
    def __init__(self, params, container):
        super(FullPageContext, self).__init__(container.request)
        if 'page_id' in params:
            self.submenu.create_menu_group('update', 'Update', '/sequences/update/%s' % (str(params['page_id'])), 'zmdi-border-all')
            # self.submenu.create_menu_group('block', 'Block', '/sequences/detail/%s/block/list' % (str(params['page_id'])), 'zmdi-border-all')
            # self.submenu.create_menu_group('comment', 'Comment', '/sequences/detail/%s/comment/list' % (str(params['page_id'])), 'zmdi-border-all')
